from django.db import models


class DoctorCategory(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name


class Doctor(models.Model):
    GENDER_CHOICE = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )
    category = models.ForeignKey(DoctorCategory, blank=True, null=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    title = models.CharField(max_length=150, blank=True, null=True)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICE)
    photo = models.ImageField()
    specialty = models.CharField(max_length=300)
    med_office_name = models.CharField(max_length=100)
    med_office_street = models.CharField(max_length=100)
    med_office_city = models.CharField(max_length=100)
    med_office_state = models.CharField(max_length=2)
    medical_training = models.TextField()
    fellowships = models.TextField()
    board_certification = models.TextField()
    is_department_head = models.BooleanField()

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)
