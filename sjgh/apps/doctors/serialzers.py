from .models import Doctor, DoctorCategory
from rest_framework import serializers


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = (
            'id',
            'category',
            'first_name',
            'last_name',
            'gender',
            'photo',
            'specialty',
            'med_office_name',
            'med_office_street',
            'med_office_city',
            'med_office_state',
            'medical_training',
            'fellowships',
            'board_certification',
            'is_department_head',
        )


class DoctorCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorCategory
        fields = (
            'id',
            'name',
            'description',
        )