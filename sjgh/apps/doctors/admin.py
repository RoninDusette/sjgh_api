from django.contrib import admin
from .models import Doctor, DoctorCategory


admin.site.register(Doctor)
admin.site.register(DoctorCategory)