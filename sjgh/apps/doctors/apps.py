from django.apps import AppConfig


class DoctorsConfig(AppConfig):
    name = 'sjgh.apps.doctors'
