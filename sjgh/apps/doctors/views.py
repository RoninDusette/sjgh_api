from rest_framework import viewsets
from .models import DoctorCategory, Doctor
from .serialzers import DoctorSerializer, DoctorCategorySerializer


class DoctorViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer


class DoctorCategoryViewSet(viewsets.ModelViewSet):
    queryset = DoctorCategory.objects.all()
    serializer_class = DoctorCategorySerializer